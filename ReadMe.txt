Members and Tasks:
___________________________________________

Ethan Manns:
* Database setup
* DML backend

Chris Pawlik:
* DML frontend
* Webpage design

Ethan Mangosing:
* Table normalization
* Quality control

Matt Gietl:
* ER Diagram
* Schema design

___________________________________________

How-To

This assumes that the user has already completed the steps given in the section on
'Setup Tomcat Web Server on Lectura' in HowTo.txt, which can be found on Lectura at:
/home/cs460/fall15/2015p4

In these instructions, we name the Tomcat installation directory as $TOMCAT_HOME.

$PORTNUMBER is the port number you have set up the tomcat server to run on in the
first part of the setup.

1. Shutdown the tomcat server if it is currently running.

   > $TOMCAT_HOME/bin/shutdown.sh

2. Replace the contents of TOMCAT_HOME/webapps/ROOT with the contents of this
   repositiory's root folder.

3. Restart the Tomcat server.

   > $TOMCAT_HOME/bin/startup.sh

4. Visit the demo application via web at:

   http://lectura.cs.arizona.edu:$PORTNUMBER

5. Shutdown the Tomcat server.

   > $TOMCAT_HOME/bin/shutdown.sh