--cleanup and setup
set autocommit off;
set define off;
DROP TABLE emanns95.office;

--create table
CREATE TABLE emanns95.office(
officeId integer primary key,
managerId integer,
street varchar2(40),
city varchar2(12),
state varchar2(12)
);
grant select on emanns95.office to public;

--populate table with sample data
insert into emanns95.office values (1102,1234,'1717 South Greenburn Circle','Bearsden','Connecticut');
insert into emanns95.office values (2258,7649,'1738 Orange Grove Street','Glasgow','New York');
insert into emanns95.office values (0128,2014,'2014 Forest Hill Drive','Jersey City','New Jersey');
commit;
