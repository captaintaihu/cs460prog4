package dbController;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import java.sql.ResultSet;
import java.util.*;

/**
 * Servlet implementation class for Servlet: DatabaseController
 */
public class DatabaseController {
	static final long serialVersionUID = 1L;
	/**
	 * A handle to the connection to the DBMS.
	 */
	protected Connection connection_;
	/**
	 * A handle to the statement.
	 */
	protected Statement statement_;
	/**
	 * The connect string to specify the location of DBMS
	 */
	protected String connect_string_ = null;
	/**
	 * The password that is used to connect to the DBMS.
	 */
	protected String password = null;
	/**
	 * The username that is used to connect to the DBMS.
	 */
	protected String username = null;

	public DatabaseController() {
		// your cs login name
		username = "emanns95";
		// your Oracle password, NNNN is the last four digits of your CSID
		password = "a2390";
		connect_string_ = "jdbc:oracle:thin:@aloe.cs.arizona.edu:1521:oracle";
	}

	/**
	 * Closes the DBMS connection that was opened by the open call.
	 */
	public void Close() {
		try {
			statement_.close();
			connection_.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		connection_ = null;
	}

	/**
	 * Commits all update operations made to the dbms. If auto-commit is on,
	 * which is by default, it is not necessary to call this method.
	 */
	public void Commit() {
		try {
			if (connection_ != null && !connection_.isClosed())
				connection_.commit();
		} catch (SQLException e) {
			System.err.println("Commit failed");
			e.printStackTrace();
		}
	}

	public void Open() {
		boolean opened = false;
		while (!opened) {
			try {
				Class.forName("oracle.jdbc.OracleDriver");
				connection_ = DriverManager.getConnection(connect_string_,
						username, password);
				statement_ = connection_.createStatement();
				opened = true;
				return;
			} catch (SQLException sqlex) {
				sqlex.printStackTrace();
				opened = false;
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
				System.exit(1); // programemer/dbsm error
			} catch (Exception ex) {
				ex.printStackTrace();
				System.exit(2);
			}
		}
	}

	// The names and the telephone numbers of the Managers of each office.
	public Vector<String> Q1() {
		String sql_query = "SELECT name,phone "
				+ "FROM emanns95.staff join emanns95.office on staffId=managerId";
		try {
			ResultSet rs = statement_.executeQuery(sql_query);
			Vector<String> result = new Vector<String>();
			while (rs.next()) {
				String temp_record = rs.getString("name") + "##"
						+ rs.getString("phone");
				result.add(temp_record);
			}
			return result;
		} catch (SQLException sqlex) {
			sqlex.printStackTrace();
		}
		return null;
	}

	// The full address of all offices in Glasgow.
	public Vector<String> Q2() {
		String sql_query = "SELECT street,city,state "
				+ "FROM emanns95.office WHERE city='Glasgow'";
		try {
			ResultSet rs = statement_.executeQuery(sql_query);
			Vector<String> result = new Vector<String>();
			while (rs.next()) {
				String temp_record = rs.getString("street") + "##"
						+ rs.getString("city") + "##" + rs.getString("state");
				result.add(temp_record);
			}
			return result;
		} catch (SQLException sqlex) {
			sqlex.printStackTrace();
		}
		return null;
	}

	// The names of all female Instructors based in the Glasgow and Bearsden
	// offices.
	public Vector<String> Q3() {
		String sql_query = "SELECT distinct name "
				+ "FROM (SELECT * "
				+ "   FROM emanns95.staff,emanns95.office "
				+ "   WHERE city='Glasgow' or city='Bearsden') "
				+ "WHERE (gender='F' and (position='Instructor' OR position='Senior Instructor'))";
		try {
			ResultSet rs = statement_.executeQuery(sql_query);
			Vector<String> result = new Vector<String>();
			while (rs.next()) {
				String temp_record = rs.getString("name");
				result.add(temp_record);
			}
			return result;
		} catch (SQLException sqlex) {
			sqlex.printStackTrace();
		}
		return null;
	}

	// The total number of staff at each office.
	public Vector<String> Q4() {
		String sql_query = "SELECT s.officeId,COUNT(staffId) as staffCount "
				+ "FROM emanns95.staff s,emanns95.office o "
				+ "WHERE s.officeId=o.officeId " + "GROUP BY s.officeId";
		try {
			ResultSet rs = statement_.executeQuery(sql_query);
			Vector<String> result = new Vector<String>();
			while (rs.next()) {
				String temp_record = rs.getString("officeId") + "##"
						+ rs.getString("staffCount");
				result.add(temp_record);
			}
			return result;
		} catch (SQLException sqlex) {
			sqlex.printStackTrace();
		}
		return null;
	}

	// The total number of clients (past and present) in each city.
	public Vector<String> Q5() {
		String sql_query = "SELECT city,count(clientId) as clientCount "
				+ "FROM emanns95.office o,emanns95.client c "
				+ "WHERE o.officeId=c.officeId " + "GROUP BY city";
		try {
			ResultSet rs = statement_.executeQuery(sql_query);
			Vector<String> result = new Vector<String>();
			while (rs.next()) {
				String temp_record = rs.getString("city") + "##"
						+ rs.getString("clientCount");
				result.add(temp_record);
			}
			return result;
		} catch (SQLException sqlex) {
			sqlex.printStackTrace();
		}
		return null;
	}

	// Outputs staff table for user
	public Vector<String> staffTable() {
		String sql_query = "SELECT * FROM emanns95.staff";
		try {
			ResultSet rs = statement_.executeQuery(sql_query);
			Vector<String> result = new Vector<String>();
			while (rs.next()) {
				String temp_record = rs.getString("staffid") + "##"
						+ rs.getString("name") + "##" + rs.getString("phone")
						+ "##" + rs.getString("gender") + "##"
						+ rs.getString("position") + "##"
						+ rs.getString("officeid");
				result.add(temp_record);
			}
			return result;
		} catch (SQLException sqlex) {
			sqlex.printStackTrace();
		}
		return null;
	}

	// Outputs office table for user
	public Vector<String> officeTable() {
		String sql_query = "SELECT * FROM emanns95.office";
		try {
			ResultSet rs = statement_.executeQuery(sql_query);
			Vector<String> result = new Vector<String>();
			while (rs.next()) {
				String temp_record = rs.getString("officeid") + "##"
						+ rs.getString("managerid") + "##"
						+ rs.getString("street") + "##" + rs.getString("city")
						+ "##" + rs.getString("state");
				result.add(temp_record);
			}
			return result;
		} catch (SQLException sqlex) {
			sqlex.printStackTrace();
		}
		return null;
	}

	// Outputs client table for user
	public Vector<String> clientTable() {
		String sql_query = "SELECT * FROM emanns95.client";
		try {
			ResultSet rs = statement_.executeQuery(sql_query);
			Vector<String> result = new Vector<String>();
			while (rs.next()) {
				String temp_record = rs.getString("clientid") + "##"
						+ rs.getString("name") + "##" + rs.getString("gender")
						+ "##" + rs.getString("officeid");
				result.add(temp_record);
			}
			return result;
		} catch (SQLException sqlex) {
			sqlex.printStackTrace();
		}
		return null;
	}

	// Insert data into Client table
	public String insertClient(String clientId, String name, String gender,
			String officeId) {
		String sql_query = "insert into emanns95.client values (" + clientId
				+ ",'" + name + "','" + gender + "'," + officeId + ")";
		try {
			int result = statement_.executeUpdate(sql_query);
			if (result != 0)
				return "Success";
			else
				return "Failure";// results will return 0 if not read as an SQL
									// DML statement
		} catch (SQLException sqlex) {
			return "Failure";
		}
	}

	// Insert data into Office table
	public String insertOffice(String officeId, String managerId,
			String street, String city, String state) {
		String sql_query = "insert into emanns95.office values (" + officeId
				+ "," + managerId + ",'" + street + "','" + city + "','"
				+ state + "')";
		try {
			int result = statement_.executeUpdate(sql_query);
			if (result != 0)
				return "Success";
			else
				return "Failure";// results will return 0 if not read as an SQL
									// DML statement
		} catch (SQLException sqlex) {
			return "Failure";
		}
	}

	// Insert data into Staff table
	public String insertStaff(String staffId, String name, String phone,
			String gender, String position, String officeId) {
		String sql_query = "insert into emanns95.staff values (" + staffId
				+ ",'" + name + "'," + phone + ",'" + gender + "','" + position
				+ "'," + officeId + ")";
		try {
			int result = statement_.executeUpdate(sql_query);
			if (result != 0)
				return "Success";
			else
				return "Failure";// results will return 0 if not read as an SQL
									// DML statement
		} catch (SQLException sqlex) {
			return "Failure";
		}
	}

	// Delete data from Client table
	public String deleteClient(String clientId) {
		String sql_query = "DELETE FROM emanns95.client WHERE clientId="
				+ clientId;
		try {
			int result = statement_.executeUpdate(sql_query);
			if (result != 0)
				return "Success";
			else
				return "Failure";// results will return 0 if not read as an SQL
									// DML statement
		} catch (SQLException sqlex) {
			return "Failure";
		}
	}

	// Delete data from Office table
	public String deleteOffice(String officeId) {
		String sql_query = "DELETE FROM emanns95.office WHERE officeId="
				+ officeId;
		try {
			int result = statement_.executeUpdate(sql_query);
			if (result != 0)
				return "Success";
			else
				return "Failure";// results will return 0 if not read as an SQL
									// DML statement
		} catch (SQLException sqlex) {
			return "Failure";
		}
	}

	// Delete data from Staff table
	public String deleteStaff(String staffId) {
		String sql_query = "DELETE FROM emanns95.staff WHERE staffId="
				+ staffId;
		try {
			int result = statement_.executeUpdate(sql_query);
			if (result != 0)
				return "Success";
			else
				return "Failure";// results will return 0 if not read as an SQL
									// DML statement
		} catch (SQLException sqlex) {
			return "Failure";
		}
	}

	// Handle input for updating client
	public String updateClient(String clientId, String name, String gender,
			String officeId) {
		String result = "";
		String compare = "Success";
		if (!name.isEmpty()) {
			if (!compare.equals(updateClientName(clientId, name)))
				result += "Failure to update client name. ";
			else
				result += "Successful in updating client name. ";
		} else
			result += "Name not updated. ";
		if (!gender.isEmpty()) {
			if (!compare.equals(updateClientGender(clientId, gender)))
				result += "Failure to update client gender. ";
			else
				result += "Successful in updating client gender. ";
		} else
			result += "Gender not updated. ";
		if (!officeId.isEmpty()) {
			if (!compare.equals(updateClientOffice(clientId, officeId)))
				result += "Failure to update client office. ";
			else
				result += "Successful in updating client office. ";
		} else
			result += "OfficeId not updated. ";
		return result;
	}

	/*
	 * //Update the clientId public String updateClientId(int clientId, int
	 * newId) { String sql_query = "UPDATE emanns95.client SET clientId="+newId+
	 * " WHERE clientId="+clientId; try { int result =
	 * statement_.executeUpdate(sql_query); if (result != 0) return "Success";
	 * //will return the row count, so can't be 0 else return
	 * "Failure";//results will return 0 if not read as an SQL DML statement }
	 * catch (SQLException sqlex) { return "Failure"; } }
	 */

	// update the client name
	public String updateClientName(String clientId, String name) {
		String sql_query = "UPDATE emanns95.client SET name=" + "'" + name
				+ "'" + " WHERE clientId=" + clientId;
		try {
			int result = statement_.executeUpdate(sql_query);
			if (result != 0)
				return "Success"; // will return the row count, so can't be 0
			else
				return "Failure";// results will return 0 if not read as an SQL
									// DML statement
		} catch (SQLException sqlex) {
			return "Failure";
		}
	}

	// update the client gender
	public String updateClientGender(String clientId, String gender) {
		String sql_query = "UPDATE emanns95.client SET gender=" + "'" + gender
				+ "'" + " WHERE clientId=" + clientId;
		try {
			int result = statement_.executeUpdate(sql_query);
			if (result != 0)
				return "Success"; // will return the row count, so can't be 0
			else
				return "Failure";// results will return 0 if not read as an SQL
									// DML statement
		} catch (SQLException sqlex) {
			return "Failure";
		}
	}

	// update the client's office
	public String updateClientOffice(String clientId, String officeId) {
		String sql_query = "UPDATE emanns95.client SET officeId=" + officeId
				+ " WHERE clientId=" + clientId;
		try {
			int result = statement_.executeUpdate(sql_query);
			if (result != 0)
				return "Success"; // will return the row count, so can't be 0
			else
				return "Failure";// results will return 0 if not read as an SQL
									// DML statement
		} catch (SQLException sqlex) {
			return "Failure";
		}
	}
}
