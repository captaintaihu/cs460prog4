--cleanup and setup
set autocommit off;
set define off;
DROP TABLE emanns95.staff;

--create table
CREATE TABLE emanns95.staff(
staffId integer primary key,
name varchar2(20),
phone integer,
gender char(1),
position varchar(20),
officeId integer
);
grant select on emanns95.staff to public;

--populate table with sample data
insert into emanns95.staff values (1234,'Tom Landry',1112223333,'M','Senior Instructor',1102);
insert into emanns95.staff values (1338,'Jim Zorn',9876543210,'M','Instructor',1102);
insert into emanns95.staff values (7846,'Tom Coughlin',1234567890,'M','Administrative Staff',2258);
insert into emanns95.staff values (2014,'Forrest Hill',8206751411,'M','Senior Instructor',0128);
insert into emanns95.staff values (1313,'Lisa Thompsett',4687324786,'F','Instructor',2258);
insert into emanns95.staff values (7649,'Lo Sims',7948215698,'F','Senior Instructor',2258);
insert into emanns95.staff values (4343,'Naya Jones',1314544545,'F','Administrative Staff',0128);
commit;
