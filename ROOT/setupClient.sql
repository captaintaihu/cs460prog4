--cleanup and setup
set autocommit off;
set define off;
DROP TABLE emanns95.client;

--create table
CREATE TABLE emanns95.client(
clientId integer primary key,
name varchar2(20),
gender char(1),
officeId integer
);
grant select on emanns95.client to public;

--populate table with sample data
insert into emanns95.client values (5530,'Candace Parker','F',1102);
insert into emanns95.client values (4938,'Sylvia Fowles','F',0128);
insert into emanns95.client values (1364,'Candice Wiggins','F',2258);
insert into emanns95.client values (6854,'Alexis Hornbuckle','F',0128);
insert into emanns95.client values (0586,'Jake Long','M',1102);
insert into emanns95.client values (5423,'Chris Long','M',2258);
insert into emanns95.client values (8965,'Matt Ryan','M',2258);
insert into emanns95.client values (3201,'Darren McFadden','M',0128);
insert into emanns95.client values (8967,'Glenn Dorsey','M',1102);
commit;
